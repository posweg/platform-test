#include "maps.h"

Maps::Maps(SDL_Renderer* render){
    int sound = false;

    if(sound == true){
        Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 );
    }
    //Set renderer
    renderer = render;

    //set the window's (renderer) dimensions
    SDL_GetRendererOutputSize(renderer,&screenWidth,&screenHeight);

    //Set unit size
	sz = screenWidth/16;

	//Set keyboard input
	currentKeyStates = SDL_GetKeyboardState(NULL);

	//Set player default position
	playerPosX = 1*sz;
	playerPosY = 1*sz;

	blockTotal = 0;
	mapWidth = 0;
	mapHeight = 0;
}

Maps::~Maps(){
    //Deallocate rows
    for(int i = 0; i < blockTotal; ++i) delete blockRect[i];
    //Deallocate
    delete blockRect;


    //Free the music
	Mix_FreeMusic( gMusic );
	gMusic = NULL;
}

int Maps::map(std::string mapName){
    //Load map
    playerPosX = 1*sz;
	playerPosY = 4*sz;

	mapWidth = sz*32;
	mapHeight = screenHeight;
/*
	gMusic = Mix_LoadMUS( "assets/sapce-odyssey.ogg" );
	if( gMusic == NULL ){
		std::cout << "Failed to load beat music! SDL_mixer Error: " << Mix_GetError() << std::endl;
	}
*/

	Texture level;
    level.setRenderer(renderer);
    SDL_Color textColor = {0xFF,0xFF,0xFF,0xFF};
    level.loadFromRendererText("level: " + mapName,textColor);

    Texture sandCastle;
    sandCastle.setRenderer(renderer);
    sandCastle.loadTexture("textures/foreground.png");
    SDL_Rect castleQuad = {0,0,sandCastle.getWidth(),sandCastle.getHeight()};

    Texture sandMiddle;
    sandMiddle.setRenderer(renderer);
    sandMiddle.loadTexture("textures/sand-middleground.png");
    SDL_Rect middleQuad = {0,0,sandMiddle.getWidth(),sandMiddle.getHeight()};
    float sandParallax = 0.5;

    Texture stars;
    stars.setRenderer(renderer);
    stars.loadTexture("textures/stars.png");
    SDL_Rect starsQuad = {0,0,stars.getWidth(),stars.getHeight()};
    float starsParallax = 0.2;

    SDL_Rect levelRect = {10,10,level.getWidth(),level.getHeight()};

	//Initialize the player, set its position, pass the map's size and set the renderer
    Player player(playerPosX,playerPosY,mapHeight,renderer);

	//Set the camera and pass the map and screen's dimensions
	Camera camera(mapWidth,mapHeight,screenWidth,screenHeight);

    //Initialize the powerup, set his position, pass the renderer and the player
    Powerup powerup(13*sz,8*sz,renderer,&player);

	//Initialize the block class and set the player and the renderer
	Block ground(0,sz*11,screenWidth*2,sz*1,renderer,&player);
	Block ground2(sz*21,sz*10,sz*11,sz*1,renderer,&player);
	Block ground3(sz*22,sz*9,sz*10,sz*1,renderer,&player);
	Block ground4(sz*30,sz*8,sz*2,sz*1,renderer,&player);
	Block ground5(sz*31,0,sz*1,sz*8,renderer,&player);
	//Block ground(renderer,&player);

	//Mix_PlayMusic( gMusic, -1 );

	bool quit = false;
	SDL_Event e;

	//Game loop
	while(quit == false){
		while(SDL_PollEvent(&e)!=0){
			if(e.type == SDL_QUIT){
				quit = true;
			}
		}
		if(currentKeyStates[SDL_SCANCODE_ESCAPE]){
            musicQuit();
            return 0;
		}

		//Clear the render and set the background color
		SDL_SetRenderDrawColor(renderer,10,20,73,0xFF);
		SDL_RenderClear(renderer);

		level.render(&levelRect);

		//Update the camera position
		camera.update(player.getRectangle().x,player.getRectangle().y);

		stars.render(&starsQuad,camera.getPosX(),starsParallax);
		sandMiddle.render(&middleQuad,camera.getPosX(),sandParallax);
		sandCastle.render(&castleQuad,camera.getPosX());

        //Print player
		if(player.print(camera.getPosX()) == 1) return 1;

		//Print the blocks the corresponding dimensions and positions and check collisions
        //for(int i = 0; i < blockTotal; i++) ground.printAndCheck(blockRect[i],camera.getPosX());
        ground.printAndCheck(camera.getPosX());
        ground2.printAndCheck(camera.getPosX());
        ground3.printAndCheck(camera.getPosX());
        ground4.printAndCheck(camera.getPosX());
        ground5.printAndCheck(camera.getPosX());

        //Print the poweup and check collisions
		powerup.printAndCheck(camera.getPosX());

		//Render
		SDL_RenderPresent(renderer);
	}
    musicQuit();
    return -1;
}

void Maps::musicQuit(){
    Mix_HaltMusic();
    //Free the music
	Mix_FreeMusic( gMusic );
	gMusic = NULL;
}
