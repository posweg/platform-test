#include"texture.h"

Texture::Texture(){
	//Initialize variables
	szW = 0;
	szH = 0;

	//Set the SDL_Texture to null
	texture = NULL;

    //Initialize the SDL_Image library
	IMG_Init(IMG_INIT_PNG);
	if(TTF_Init() == -1) std::cout << TTF_GetError() << std::endl;

	font = TTF_OpenFont("cooper-hewitt-medium.otf",30);
}

void Texture::setRenderer(SDL_Renderer* render){
    //Set renderer
	renderer = render;
}

Texture::~Texture(){
    //Deallocate the texture when the class closes
    free();

    IMG_Quit();
    TTF_Quit();
}

void Texture::loadTexture(std::string path){
	//Get rid of preexisting texture
	free();

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if(loadedSurface == NULL){
		std::cout << "Couldn't load " << path.c_str() << std::endl;
	}
	else{
		//Create texture from surface pixels
        	texture = SDL_CreateTextureFromSurface
					(renderer,loadedSurface);

		if(texture == NULL){
			std::cout << "Couldn't create texture from " << path.c_str() << std::endl;
		}
		else{
			//Get image dimensions
			szW = loadedSurface->w;
			szH = loadedSurface->h;
		}
		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}
}

bool Texture::loadFromRendererText(std::string textureText, SDL_Color textColor){
    //Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(font,textureText.c_str(),textColor);
	if( textSurface == NULL )
	{
		std::cout << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError() << std::endl;
	}
	else
	{
		//Create texture from surface pixels
        texture = SDL_CreateTextureFromSurface( renderer, textSurface );
		if( texture == NULL )
		{
			std::cout << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << std::endl;
		}
		else
		{
			//Get image dimensions
			szW = textSurface->w;
			szH = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}

	//Return success
	return texture != NULL;
}

void Texture::free(){
	//Free texture if it exists
	if(texture != NULL){
		SDL_DestroyTexture(texture);
		texture = NULL;
		szW = 0;
		szH = 0;
	}
}

void Texture::render(SDL_Rect* quad,SDL_Rect* frame){
    //Render the texture
	SDL_RenderCopy(renderer,texture,frame,quad);
}

void Texture::render(SDL_Rect* quad){
    //Make a frame of the size of the quad
    SDL_Rect frame = {0,0,quad->w,quad->h};
    //Render the texture
	SDL_RenderCopy(renderer,texture,&frame,quad);
}

void Texture::render(SDL_Rect* quad, int cameraX){
    //Make a frame of the size of the quad
    SDL_Rect frame = {0,0,quad->w,quad->h};

    quad->x = quad->x - cameraX;

    //Render the texture
	SDL_RenderCopy(renderer,texture,&frame,quad);

	quad->x = quad->x + cameraX;
}

void Texture::render(SDL_Rect* quad, int cameraX,float parallax){
    //Make a frame of the size of the quad
    SDL_Rect frame = {0,0,quad->w,quad->h};

    quad->x = quad->x - cameraX * parallax;

    //Render the texture
	SDL_RenderCopy(renderer,texture,&frame,quad);

	quad->x = quad->x + cameraX * parallax;
}

/*void Texture::render(SDL_Rect* quad, int cameraX,int parallax, int vel){
    //Make a frame of the size of the quad
    SDL_Rect frame = {0,0,quad->w,quad->h};

    quad->x -= cameraX / parallax;

    quad->x += vel;

    if(quad->x >= 160){
        quad->x -= 160 + szW;
        if(quad->x >= 0){
            quad->x += 160 + szW;
        }
        else SDL_RenderCopy(renderer,texture,&frame,quad);

        quad->x += 160 + szW;
    }

    //Render the texture
	SDL_RenderCopy(renderer,texture,&frame,quad);

	quad->x += cameraX / parallax;
}*/

int Texture::getWidth(){
	return szW;
}

int Texture::getHeight(){
	return szH;
}
