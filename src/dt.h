//Delta time class header

#ifndef __DT_H_INCLUDED__
#define __DT_H_INCLUDED__

#include<SDL.h>

class DeltaTime{
	public:
		DeltaTime();
		float getDt();
	private:
		float now, last, dt;
};

#endif
