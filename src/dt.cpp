//Delta time class body

#include"dt.h"

DeltaTime::DeltaTime(){
	now = SDL_GetPerformanceCounter();
}

float DeltaTime::getDt(){
	last = now;
	now = SDL_GetPerformanceCounter();

	dt = (now - last)/SDL_GetPerformanceFrequency();
	if(dt > 0.15f) dt = 0.15f;

	return dt;
}
