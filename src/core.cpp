//Core class body

#include"core.h"

void Core::init(){
	SDL_Init(SDL_INIT_VIDEO);
	gWindow = SDL_CreateWindow("Platform Test!",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_SHOWN);
	gRenderer = SDL_CreateRenderer(gWindow,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(gRenderer,0xFF,0xFF,0xFF,0xFF);
}

void Core::close(){
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	SDL_Quit();
}

int Core::coreInit(){
	init();

	Maps maps(gRenderer);

	int gamestate = 0;

	//-1 = quit, 0 = Menu, 1 = playing,
	while(gamestate != -1){
        if(gamestate == 0) gamestate = menu(gRenderer);
        else if(gamestate == 1) gamestate = maps.map("overworld");
        else gamestate = -1;

	}

	close();
	return 0;
}
