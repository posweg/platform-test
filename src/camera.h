//Camera class header

#ifndef __CAMERA_H_INCLUDED__
#define __CAMERA_H_INCLUDED__

#include"entity/player.h"

class Camera{
	public:
		Camera(int mapWidth, int mapHeight,int screenWidth, int screenHeight);
		void update(int playerX, int playerY);
		int getPosX();
		int getPosY();
	private:
		int posX, posY;
		int mW, mH;
        int scW, scH;
};

#endif
