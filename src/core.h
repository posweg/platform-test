//Core class header

#ifndef __CORE_H_INCLUDED__
#define __CORE_H_INCLUDED__

#include<iostream>
#include<SDL.h>
#include"menu.h"
#include"maps.h"

class Core{
	public:
		int coreInit();
	private:
		void init();
		void close();

		void setRectSize(int rect[],int x, int y, int w, int h);

		const int SCREEN_WIDTH = 640;
		const int SCREEN_HEIGHT = 480;

		SDL_Window* gWindow = NULL;
		SDL_Renderer* gRenderer = NULL;
};

#endif
