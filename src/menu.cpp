#include "menu.h"

class Button{
	public:
		Button(int x,int y,int w,int h,SDL_Renderer* render,std::string buttonText);
		void render();
		void hover();
		SDL_Rect getRect();
	private:
		SDL_Rect buttonRect;
		SDL_Rect textRect;
		SDL_Renderer* renderer;
		Texture text;
		bool isHover;
};

Button::Button(int x,int y,int w,int h,SDL_Renderer* render,std::string buttonText){
	renderer = render;
	buttonRect = {x,y,w,h};
	isHover = false;

	text.setRenderer(renderer);
	SDL_Color textColor = {0xFF,0xFF,0xFF,0xFF};
	text.loadFromRendererText(buttonText,textColor);

	textRect = {x+(w/2)-text.getWidth()/2,y+(h/2)-text.getHeight()/2,text.getWidth(),text.getHeight()};
}

void Button::render(){
	if(isHover) SDL_SetRenderDrawColor(renderer,80,80,80,0xFF);
	else SDL_SetRenderDrawColor(renderer,128,128,128,0xFF);

	SDL_RenderFillRect(renderer,&buttonRect);
	text.render(&textRect);
	isHover = false;
}

void Button::hover(){
	isHover = true;
}

SDL_Rect Button::getRect(){
	return buttonRect;
}

int menu(SDL_Renderer* renderer){
	int SCREEN_WIDTH, SCREEN_HEIGHT;
	SDL_GetRendererOutputSize(renderer,&SCREEN_WIDTH,&SCREEN_HEIGHT);

	bool quit = false;
	SDL_Event e;

	Texture txLogo;
	txLogo.setRenderer(renderer);
	txLogo.loadTexture("textures/title.png");

	int logoVmargin = 40;

	SDL_Rect logo = {(SCREEN_WIDTH/2)-txLogo.getWidth()/2,logoVmargin,txLogo.getWidth(),txLogo.getHeight()};

	int btnSpc = SCREEN_HEIGHT - (logo.y + logo.h);
	int buttonNum = 3;
	int btnWidth = SCREEN_WIDTH/3;
	int btnHeight = btnSpc/buttonNum/2;
	int spc = (btnSpc-buttonNum*btnHeight)/(buttonNum+1);

	Button playBtn(
		SCREEN_WIDTH/2 - btnWidth/2,
		(1*spc + 0*btnHeight) + SCREEN_HEIGHT - btnSpc,
		btnWidth, btnHeight,
		renderer,
		"play"
	);
	Button optionsBtn(SCREEN_WIDTH/2-btnWidth/2,(2*spc+1*btnHeight)+SCREEN_HEIGHT-btnSpc,btnWidth,btnHeight,renderer,"options");
	Button quitBtn(SCREEN_WIDTH/2-btnWidth/2,(3*spc+2*btnHeight)+SCREEN_HEIGHT-btnSpc,btnWidth,btnHeight,renderer,"quit");

	Texture txtVer;
	txtVer.setRenderer(renderer);
	SDL_Color txtVerColor = {0xFF,0xFF,0xFF,0xFF};
	txtVer.loadFromRendererText("Version 0.2",txtVerColor);
	SDL_Rect txtVerRect = {10,SCREEN_HEIGHT-txtVer.getHeight()-10,txtVer.getWidth(),txtVer.getHeight()};

	int select = 0;
	bool wait = 0;
	const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);

	while(!quit){
		while(SDL_PollEvent(&e)!=0){
			if(e.type == SDL_QUIT){
				quit = true;
			}
		}
		SDL_SetRenderDrawColor(renderer,100,200,200,0xFF);
		SDL_RenderClear(renderer);

		int dir = 0;
		if(currentKeyStates[SDL_SCANCODE_UP]) dir--;
		if(currentKeyStates[SDL_SCANCODE_DOWN])dir++;

		if(dir != 0){
			if(!wait) select += dir;
			wait = true;
		}
		else wait = false;

		if(select >= buttonNum) select = 0;
		if(select < 0) select = buttonNum-1;

		txLogo.render(&logo);

		if(currentKeyStates[SDL_SCANCODE_SPACE]){
			if(select ==  0) return 1;
			else if(select == 1) return 0;
			else if(select == 2) break;
		}

		if(select == 0) playBtn.hover();
		else if(select == 1) optionsBtn.hover();
		else quitBtn.hover();

		playBtn.render();
		optionsBtn.render();
		quitBtn.render();

		txtVer.render(&txtVerRect);

		SDL_RenderPresent(renderer);
	}

	return -1;
}
