//Texture management class header

#ifndef __TEXTURE_H_INCLUDED__
#define __TEXTURE_H_INCLUDED__

#include<SDL.h>
#include<SDL_image.h>
#include<SDL_ttf.h>
#include<iostream>
#include<string>

class Texture{
	public:
		Texture();
		~Texture();
		void loadTexture(std::string path);
		bool loadFromRendererText(std::string textureText, SDL_Color textColor);

		void free();
		void render(SDL_Rect* quad,SDL_Rect* frame);
		void render(SDL_Rect* quad);
		void render(SDL_Rect* quad, int cameraX,float parallax);
		void render(SDL_Rect* quad, int cameraX);
		//void render(SDL_Rect* quad, int cameraX,int parallax, int vel);

		int getWidth();
		int getHeight();
		void setRenderer  (SDL_Renderer* render);
	private:
		SDL_Texture* texture;
		SDL_Renderer* renderer;
		TTF_Font *font = NULL;

		int szW, szH;
};

#endif
