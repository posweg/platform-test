#ifndef MAPS_H
#define MAPS_H

#include<iostream>
#include<SDL.h>
#include"entity/player.h"
#include"entity/block.h"
#include"entity/powerup.h"
#include"camera.h"
#include<fstream>
#include<algorithm>
#include"texture.h"
#include<SDL_mixer.h>
#include<vector>

class Maps{
    public:
        Maps(SDL_Renderer* render);
        ~Maps();
        int map(std::string mapName);
    private:
        void loadMap(std::string path);
        void musicQuit();

        SDL_Renderer* renderer;

        int blockTotal;
        int** blockRect;
        int sz;
        int screenWidth, screenHeight;
        int mapWidth, mapHeight;
        int playerPosX, playerPosY;
        const Uint8* currentKeyStates;

        Mix_Music *gMusic = NULL;
};

#endif // MAPS_H
