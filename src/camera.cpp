//Camera class body

#include"camera.h"

Camera::Camera(int mapWidth, int mapHeight,int screenWidth, int screenHeight){
    mW = mapWidth;
    mH = mapHeight;
	scW = screenWidth;
	scH = screenHeight;
}

void Camera::update(int playerX, int playerY){
	//Make the camera move when the player hits the middle of the screen
	if(mW != scW){
        	if(playerX >= scW/2)
			posX = playerX - scW/2;

	        else
			posX = 0;

        	if(playerX >= mW - scW/2)
			posX = mW - scW;
	}
	else posX = 0;

	if(mH != scH){
        	if(playerY >= scH/2)
			posY = playerY - scH/2;

	        else
			posY = 0;

        	if(playerY >= mH - scH/2)
			posY = mH - scH;
	}
	else posY = 0;
}

int Camera::getPosX(){
	return posX;
}

int Camera::getPosY(){
	return posY;
}
