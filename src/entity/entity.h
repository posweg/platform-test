//Base class for entities

#ifndef __ENTITY_H_INCLUDED__
#define __ENTITY_H_INCLUDED__

#include<SDL.h>
#include<iostream>

class Entity{
	public:
		SDL_Rect getRectangle();
	protected:
		SDL_Rect rect;
		SDL_Renderer** renderer;
		int posX, posY, szW, szH;
		int type;
};

#endif
