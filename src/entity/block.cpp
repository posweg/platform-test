#include"block.h"

Block::Block(int x,int y,int w,int h,SDL_Renderer* render, Player* ply){
    renderer = render; //Set the renderer
    player = ply; //Set the player class to check

    type = 1; //Set the entity type (1 = block)

    rect = {x,y,w,h};
}

/*void Block::printAndCheck(int dimensions[], int cameraX){
    //Set the block's rect's dimensions and position
    rect = {dimensions[0],dimensions[1],dimensions[2],dimensions[3]}

    //Check if it collides with the player
    player->check(rect,type);

    //Adjust de SDL_Rect x to follow the camera
    rect.x = dimensions[0] - cameraX;

	//Set render color and render the rectangle
	SDL_SetRenderDrawColor(renderer,169,145,73,0xFF);
	SDL_RenderFillRect(renderer,&rect);
}*/

void Block::printAndCheck(int cameraX){
    //Check if it collides with the player
    player->check(rect,type);

    //Adjust de SDL_Rect x to follow the camera
    rect.x -= cameraX;

	//Set render color and render the rectangle
	/*SDL_SetRenderDrawColor(renderer,0,0xFF,0,0xFF);
	SDL_RenderDrawRect(renderer,&rect);*/

	rect.x += cameraX;
}
