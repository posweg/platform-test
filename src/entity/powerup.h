//Powerup class header

#ifndef __POWERUP_H_INCLUDED__
#define __POWERUP_H_INCLUDED__

#include<SDL.h>
#include<iostream>
#include"entity.h"
#include"player.h"

class Entity;

class Powerup: public Entity{
	public:
		Powerup(int x, int y,SDL_Renderer* render, Player* ply);
		void printAndCheck(int cameraX);
	protected:
		Player* player;
		SDL_Renderer* renderer;
};

#endif
