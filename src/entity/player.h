//Player class header<

#ifndef __PLAYER_H_INCLUDED__
#define __PLAYER_H_INCLUDED__

#include<SDL.h>
#include<iostream>
#include<cmath>
#include"../dt.h"
#include"entity.h"
#include"../texture.h"

class Entity;

class Player: public Entity{
	public:
        Player(int x,int y,int lvlH,SDL_Renderer* render);
		int print(int cameraX);
		int check(SDL_Rect rectA,int type);
	private:
		void loadMedia();
		void move();
		void die();
		int intVelX();

		SDL_Rect plyFrame[3];
		SDL_Rect plyRun;
		SDL_Renderer* renderer;
		DeltaTime dTime;
		Texture ply;

		bool ground, topCollision;
		bool isRunning = false;
		bool ifRunning = false;

		float velocityX = 0;
		float velocityY = 0;

		//int posX, posY;
		int initPosX, initPosY;
		int oldPosX, oldPosY;
		int power;
		int levelHeight;
};

#endif
