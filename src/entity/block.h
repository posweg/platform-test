//Block class header

#ifndef __BLOCK_H_INLCUDED__
#define __BLOCK_H_INCLUDED__

#include"entity.h"
#include<SDL.h>
#include<iostream>
#include"player.h"

class Entity;

class Block: public Entity{
	public:
		Block(int x,int y,int w,int h,SDL_Renderer* render, Player* ply);
		//Block(SDL_Renderer* render, Player* ply);
		void printAndCheck(int cameraX);
	protected:
		Player* player;
		SDL_Renderer* renderer;
};

#endif
