//Powerup class body

#include"powerup.h"

/*Powerup::Powerup(int x, int y, SDL_Renderer** render,Player* player, int cameraX){
	rect = {x,y,20,20};

	renderer = render;

	type = 2;

	//New SDL_Rect to make the objects follow the camera
	SDL_Rect cameraFix = rect;
	cameraFix.x -= cameraX;

	//Set render color and render the rectangle
	if(player->check(rect,type)==0){
	SDL_SetRenderDrawColor(*renderer,0xFF,0xFF,0,0xFF);
	SDL_RenderFillRect(*renderer,&cameraFix);
	}
	//posu->check(rect, type);
}*/

Powerup::Powerup(int x,int y,SDL_Renderer* render, Player* ply){
    renderer = render; //Set the renderer
    player = ply; //Set the player class to check

    type = 2; //Set the entity type (2 = powerup)

    rect = {x,y,20,20};//Set the powerup's position and dimensions
}

void Powerup::printAndCheck(int cameraX){
    //Check if it collides with the player
    if(player->check(rect,type) == 0){

    //Adjust de SDL_Rect x to follow the camera
    rect.x -= cameraX;

    //Set render color and render the rectangle
	SDL_SetRenderDrawColor(renderer,0xFF,0xFF,0,0xFF);
	SDL_RenderFillRect(renderer,&rect);

	//Change again the x position of rect
	rect.x += cameraX;
	}
}
