![alt text](https://github.com/posweg/platform-test/raw/master/textures/title.png "Platform test!")
=====

This is my first semi-big or not-so-small project, made to get my hands dirty with SDL2, have fun and experiment how feels to make an actual game, with its structure and stuff.

So far I have implemented a regular platformer, with its player, player physics, powerups, walls/blocks, and a menu system. It's all messy and clunky but hey, it's my first time and I'm doing what I can so have patience with it if you watch some huge stupid mistake thingy. And of course if you see one of those, tell me, correct it and make a pull request or put an issue!

## Controls

**In the menu:**

- **UP** and **DOWN** to navigate
- **SPACE** to accept

**PD:** the first button is to play, the third one quits the game and the second one... does nothing.

**In-game:**

- **ARROWS** To move around
- **SPACE** To jump
- **LSHIFT** To run

## Compiling

Dependencies:

- SDL2
- SDL2_ttf
- SDL2_image
- SDL2_mixer

To compile it just follow the standard CMake procedure:

    mkdir build && cd build
    cmake ../
    make
    make install

## References

Lazy Foo's SDL guide: http://lazyfoo.net/tutorials/SDL/index.php

Jorge Rodrigez's YouTube series on Math for Game Developers: https://www.youtube.com/playlist?list=PLW3Zl3wyJwWOpdhYedlD-yCB7WQoHf-My

Khan Academy's algebra series: https://www.youtube.com/watch?v=kpCJyQ2usJ4&list=PL7AF1C14AF1B05894
